## Task

The challenge is to design an algorithm in python that is finding the smallest circle around a cluster of points in 2D.
Python version 3.7 or compatible should be used. There should be instructions to install all required dependencies (preferable in a virtual environment). To start the algorithm there should be a “start.py” that can be run from the python interpreter.
Input data is an array of points, each point consists of a X and a Y coordinate. (example data below)
By default, a random number of random points should be generated with each call of the script (25 to 60 points, x/y in range -15.0 to +15.0).
The circle is defined with a center point (consisting of a X and a Y coordinate) and a radius.
All points must be within this circle, while the radius of this circle should be as small as possible.
The output results should be plotted on a 2D plane, with all points and the result circle.
This challenge is only considering two dimensions, X and Y.

The algorithm is evaluated according to those criteria:
    - Code readability
    - Code documentation
    - Algorithm efficiency
    - Code efficiency
    - Choice of 3rd. party dependencies
    - Code size

Example points:
[
    {x: 2, y: 1},
    {x: 5, y: 5},
    {x: 9, y: 2},
    {x: 5, y: 1},
    {x: 2, y: 5},
    {x: 1, y: 1},
    {x: 6, y: 2},
    {x: 3, y: 5},
    {x: 5, y: 11},
    {x: 3, y: 12},
    {x: 8, y: 7},
    {x: 1, y: 5},
    {x: 2, y: 2},
    {x: 3, y: 1},
    {x: 6, y: 4},
]


## Minium install and run instructions
    
 - Create virtual environment using `mkvirtualenv mincircle`. 
 - Go to project's directory (continuity_test) and run `pip install -r requirements-cv.txt`.
 - Run `python3 start.py`.
 
  Program will run and show the result for both type of implementations. At the end it will draw points and minimal enclosing circle. 
 
 ---
 
*If we wouldn't use opencv lib it is only `pip install requirements.txt` needed and code change to disable it.

### Resources

https://www.geeksforgeeks.org/program-find-circumcenter-triangle-2/
https://www.wikiwand.com/en/Circumscribed_circle
https://math.wikia.org/wiki/Circumscribed_circle
http://www.batesville.k12.in.us/Physics/APPhyNet/Dynamics/Center%20of%20Mass/2D_1.html
https://www.nayuki.io/page/smallest-enclosing-circle