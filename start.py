import math
import random
import time

import cv2
import matplotlib.pyplot as plt
import numpy as np

import nayuki


class Point:
    def __init__(self, x: float, y: float):
        self.x = x
        self.y = y

    def __str__(self):
        return "[{0}, {1}]".format(self.x, self.y)

    def __repr__(self):
        return "[{0}, {1}]".format(self.x, self.y)


class Circle:
    """ We expect """

    def __init__(self, center: Point, radius, a: Point = None, b: Point = None, c: Point = None):
        self.o = center
        self.radius = radius
        self.a = a
        self.b = b
        self.c = c

    @classmethod
    def from_points(cls, a: Point, b: Point, c: Point):
        d = 2 * (a.x * (b.y - c.y) + b.x * (c.y - a.y) + c.x * (a.y - b.y))

        if d == 0.0:
            return None

        # Circumscribed center of a triangle
        o = Point(
            x=((a.x * a.x + a.y * a.y) * (b.y - c.y) + (b.x * b.x + b.y * b.y) * (c.y - a.y) + (
                    c.x * c.x + c.y * c.y) * (a.y - b.y)) / d,
            y=((a.x * a.x + a.y * a.y) * (c.x - b.x) + (b.x * b.x + b.y * b.y) * (a.x - c.x) + (
                    c.x * c.x + c.y * c.y) * (b.x - a.x)) / d
        )

        radius = distance(o, a)
        return cls(o, radius, a, b, c)

    def __str__(self):
        return "{0}, {1}".format(self.o, self.radius)


def distance(a: Point, b):
    d = math.sqrt(math.pow(a.x - b.x, 2) + math.pow(a.y - b.y, 2))
    return d


def generate_points(min_number_points=25, max_number_points=60, min_xy=-15, max_xy=15):
    """Generate between 25 and 60 number of points in XY range between -15 to 15 by default"""

    number_of_points = random.randint(min_number_points, max_number_points)
    points = []

    for point in range(number_of_points):
        x = random.randint(min_xy, max_xy)
        y = random.randint(min_xy, max_xy)
        point = Point(x, y)
        points.append(point)

    print(points)
    return points


def get_center_of(points):
    x_sum = 0
    y_sum = 0
    for point in points:
        x_sum += point.x
        y_sum += point.y

    center = Point(x_sum / len(points), y_sum / len(points))
    return center


def opencv_calculate_circle(points):
    # Convert list of points to np.array
    _points = np.array(list(map(lambda point: [point.x, point.y], points)))

    (x, y), radius = cv2.minEnclosingCircle(_points)

    cv_circle = Circle(Point(x, y), radius)
    return cv_circle


def draw_graph(points, circle):
    """ Draw graph with points and circle"""

    fig, ax = plt.subplots()  # note we must use plt.subplots, not plt.subplot

    # Draw dots
    x_data = []
    y_data = []

    for point in points:
        x_data.append(point.x)
        y_data.append(point.y)
    ax.scatter(x_data, y_data)

    # Draw circle
    circle = plt.Circle((circle.o.x, circle.o.y), circle.radius, facecolor=(0, 0, 0, 0.125), edgecolor=(1.0, 0, 0))
    ax.add_artist(circle)

    plt.title("Minimal circle")
    ax.set(xlabel="X", ylabel="Y")
    ax.axis("scaled")
    ax.set_ylim([-25, 25])
    ax.set_xlim([-25, 25])
    ax.grid()
    plt.show()


def calculate_min_circle(points):
    """Custom approach"""

    # FIXME it differentiate from the exact result works for some data sets

    p_x = [point.x for point in points]
    p_y = [point.y for point in points]

    center = Point((max(p_x) + min(p_x)) / 2, (max(p_y) + min(p_y)) / 2)
    # print(center)

    list_radius = [distance(center, point) for point in points]
    # print(list_radius)

    radius = max(list_radius)
    return Circle(center, radius)


def main():
    points = generate_points(25, 60)

    # Reformat points for nayuki module
    _points = [(point.x, point.y) for point in points]

    # Init circle
    min_circle = Circle(Point(0, 0), 0)

    start = time.process_time_ns()
    min_circle = opencv_calculate_circle(points)
    end = time.process_time_ns()
    print("Time elapsed opencv: {}".format(end - start))
    start = time.process_time_ns()
    min_custom = calculate_min_circle(points)
    end = time.process_time_ns()
    print("Time elapsed custom: {}".format(end - start))
    start = time.process_time_ns()
    mc = nayuki.make_circle(_points)
    end = time.process_time_ns()
    print("Time elapsed nayuki: {}".format(end - start))

    print("Custom-solution:{}".format(min_custom))
    print("OpenCv:{}".format(min_circle))
    print("Nayuki:{}".format(mc))

    draw_graph(points, min_circle)
    # Uncoment for comparison
    # draw_graph(points, min_custom)


if __name__ == '__main__':
    main()
